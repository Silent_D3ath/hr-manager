import axios from "axios";
import * as https from "https";
import { config } from '../config'
var jwt = require('jsonwebtoken');

const customAxios = axios.create({
  httpsAgent: new https.Agent({
    rejectUnauthorized: false,
    keepAlive: true,
    timeout: 15000
  })
});
let responseFromCertEndpoint: any;
export const getJWKS = async (): Promise<any> => {
  try {
    let resultTypesResponse = await customAxios.get<any>(`${config.authenticationApi}/auth/realms/daimler/protocol/openid-connect/certs`)
    responseFromCertEndpoint = resultTypesResponse.data;
    return responseFromCertEndpoint
  } catch (error: any) {
    console.log("getJWKS", error)
    throw new Error(error.response)
  }
}
const authCheck = async (req: any, res: any, next: any) => {  
  try {
    let responseFromJWKS = await(getJWKS())
    if(responseFromJWKS) next();
  } catch (error) {
    console.log(error)
    return res.status(403).send('No Access Token!');
  }
}
export { authCheck };