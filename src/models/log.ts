import { Document, Model, model, Schema } from 'mongoose';
const LogSchema = new Schema({ 
 
  description: {
    type: String,
    index: true,
    required: true
  }
  
});

// export type efpTestCaseResultModel = Model<efpTestCaseResultDocument>;

export default model<any>('Logs', LogSchema);
