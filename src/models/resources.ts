import { Document, Model, model, Schema } from 'mongoose';
const ResourcesSchema = new Schema({ 
  name: {
    type: String,
    index: true,
    required: true
  },
  firstName: {
    type: String,
    index: true,
    required: true
  },
  lastName: {
    type: String,
    index: true,
    required: true
  },
  department: {
    type: String,
    index: true,
    required: true
  },
  salary: {
    type: Number,
    index: true,
    required: true
  },
  gender: {
    type: String,
    index: true,
    required: true
  },
});

// export type efpTestCaseResultModel = Model<efpTestCaseResultDocument>;

export default model<any>('Resources', ResourcesSchema);
