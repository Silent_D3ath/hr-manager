
const axios = require("axios");
const https = require("https");
import { config } from '../config'
import LOGSCHEMA from '../models/log'
const customAxios = axios.create({
  timeout: 120000,
  httpsAgent: new https.Agent({
    rejectUnauthorized: false
  })
});

export const newLog = async (description: string) => {  
  try {    
    await LOGSCHEMA.create({"description": description})
  } catch (error: any) {
    console.log("newLog", error)
  } 
}
