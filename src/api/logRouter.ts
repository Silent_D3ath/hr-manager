import express from 'express';
import LOGS from '../models/log';
const logsRouter = express.Router();


logsRouter.get('/', async (req, res) => {
  
    // #swagger.summary = 'Get Logs'
    // #swagger.description = "Retrace every activity in the system"
  try {
    let response = await LOGS.find({})
    res.status(200).send(response);
  } catch (err: any) {
    res.status(404).json(err.message);
  } 
});
module.exports = logsRouter;