import express from 'express';
const HRRouter = express.Router();
import * as hrManager from '../code/hrManager'
import { iResource } from '../interfaces/resources';

HRRouter.post('/', async (req, res) => {
  // #swagger.summary = 'Create a new resource'
  // #swagger.description = "Creates a new resource and calculates the salary based on the department"
  /* #swagger.parameters["payload"] = {
                in: 'body',
                description: 'resourse metadata',
                schema: {
                  "name": "John Doe",
                  "firstName": "John",
                  "lastName": "Doe",
                  "salary": 100000,
                  "department": "RD",
                  "gender": "male"
                }              
        } */
  try {
    const result = await hrManager.createNewResource(req.body)
    res.status(200).send(result);
  } catch (err: any) {
    res.status(404).json(err.message);
  } 
});
HRRouter.put('/salary/:name', async (req, res) => {
  
  // #swagger.summary = 'Increase salary'
      // #swagger.description = "Give a raise to a devoted workforce"
  try {
    const result = await hrManager.updateSalary(req.params.name, req.body.salary)
    res.status(200).send(result);
  } catch (err: any) {
    res.status(404).json(err.message);
  } 
});
HRRouter.put('/:name', async (req, res) => {  
      // #swagger.summary = 'Update user'
      // #swagger.description = "In case any information has changed"
  try {
    const result = await hrManager.updateResource(req.body)
    res.status(200).send(result);
  } catch (err: any) {
    res.status(404).json(err.message);
  } 
});
HRRouter.get('/', async (req, res) => {
    // #swagger.summary = 'Returns resources'
  // #swagger.description = "Return all resources matching certain parameters"
 
  try {
    const result = await hrManager.returnEmployees(req.query.name, req.query.firstName, req.query.lastName, req.query.salary, req.query.department)
    res.status(200).send(result);
  } catch (err: any) {
    res.status(404).json(err.message);
  } 
});
HRRouter.delete('/:name', async (req, res) => {  
    // #swagger.summary = 'Remove Employee'
    // #swagger.description = "Fire that little scumbag!"
  try {
    const result = await hrManager.fireEmployee(req.params.name)
    res.status(200).send(result);
  } catch (err: any) {
    res.status(404).json(err.message);
  } 
});




module.exports = HRRouter;