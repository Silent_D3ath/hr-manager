import express from 'express';
const BasicRouter = express.Router();

BasicRouter.get("/ping", async (req, res) => {	
			res.status(200).send({
				version: "1.0.0",
				department: "HR",
				description: "You are only a number to us",
				Status: "Live"
				
			});
});

module.exports = BasicRouter;