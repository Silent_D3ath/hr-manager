export interface iResource {
  name: string;
  firstName: string;
  lastName: string;
  department: string;
  salary: number;
  gender: string;
}