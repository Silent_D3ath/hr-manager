import * as fs from 'fs';
import YAML from 'yaml';

let config: any = {};
try {
  const configstring = fs.readFileSync('./config.yml').toString();
  console.log('config is: ', configstring);
  if (configstring) config = YAML.parse(configstring);
} catch (err: any) {
  console.log(err.message);
}
export { config };