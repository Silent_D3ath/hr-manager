import * as log from '../services/log'
import { iResource } from "../interfaces/resources";
import RESOURCES from '../models/resources'

export const createNewResource = async (payload: iResource): Promise<string> => {
  
  try {
    payload.salary = calculateSalary(payload.department, payload.salary)
    await RESOURCES.create(payload)
    log.newLog(`Successfully created new Resource ${payload.name}`)
   return `Created new employee`
  } catch (error: any) {
    console.log("ERROR", error)
    throw new Error(error)
  }
}
export const returnEmployees = async ( name?: any, firstName?: any, lastName?: any, salary?: any, department?: any): Promise<iResource[]> => {
  
  try {
    let searchPayload: any = {};   
    if(name ) searchPayload.name = name;
    if(firstName ) searchPayload.firstName = firstName;
    if(lastName ) searchPayload.lastName = lastName;
    if(salary ) searchPayload.salary = salary;
    if(department ) searchPayload.department = department;
    return RESOURCES.find(searchPayload)
  } catch (error: any) {
    console.log("ERROR", error)
    throw new Error(error)
  }
}
export const fireEmployee = async (firstName: string): Promise<string> => {
    
  try {
    await RESOURCES.deleteMany({firstName: firstName})
    log.newLog(`Successfully deleted employee ${firstName}`)
   return `Successfully deleted employee`
  } catch (error: any) {
    console.log("ERROR", error)
    throw new Error(error)
  }
}
export const updateSalary = async (firstName: string, salary: number): Promise<string> => {
  try {
    await RESOURCES.updateOne({firstName: firstName}, {salary: salary}) 
    log.newLog(`Updated salary of ${firstName}`)  
    return "Successfully updated salary of resource"
  } catch (error: any) {
    console.log("ERROR", error)
    throw new Error(error)
  }
}
export const updateResource = async (payload: iResource): Promise<string> => {
  try {
    await RESOURCES.updateOne({name: payload.name}, payload) 
    log.newLog(`Updated Metadata of ${payload.firstName}`)  
    return "Successfully updated resource"
  } catch (error: any) {
    console.log("ERROR", error)
    throw new Error(error)
  }
}

const calculateSalary = (department: string, salary: number): number => {
  switch(department) {
    case "RD" : return salary*1.3;
    case "HR": return salary*1.5;
    case "LEET": return salary*10
    default: 
     return salary
  }
}