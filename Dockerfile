FROM node:16-alpine
LABEL description="HR Manager für CodingTest"
LABEL maintainer="simon.hausmann@daimler.com"
LABEL version="v3"

RUN mkdir -p /usr/src/app
RUN chmod 777 /usr/src/app

WORKDIR /usr/src/app
ENV HTTP_PROXY=http://proxy.ideea.app.corpintra.net:3128
ENV HTTPS_PROXY=http://proxy.ideea.app.corpintra.net:3128
RUN npm config set proxy $HTTP_PROXY
RUN npm config set https-proxy $HTTPS_PROXY

# Install dependencies
COPY package.json .
# COPY swagger.json ./dist/swagger.yaml
COPY . .
# RUN rm ./config/config.json
# RUN cp ./config/config-prod.json ./config/config.json
# ENV NODE_EXTRA_CA_CERTS=/usr/src/app/certs/Corp-Root-CA-G2.pem
RUN yarn install

EXPOSE 3005

ENV HTTP_PROXY=
ENV HTTPS_PROXY=
CMD [ "npm", "run", "debug" ]