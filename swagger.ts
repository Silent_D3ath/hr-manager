
const swaggerAutogen = require('swagger-autogen')();

const doc = {
  info: {
    title: 'HR Manager',
    description: 'Manage your employees with an amazing backend',
  },
  host: "localhost:3005",
  schemes: ['http'],
};

const outputFile = './swagger.json';
const endpointsFiles = ['./index.ts'];
/* NOTE: if you use the express Router, you must pass in the 
   'endpointsFiles' only the root file where the route starts,
   such as index.js, app.js, routes.js, ... */

swaggerAutogen(outputFile, endpointsFiles, doc).then(() => {
  
})