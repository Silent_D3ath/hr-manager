# HR Resources

Typescript, express, mongodb, swagger

## Description

The coding example is about a human resource management system where you can create, update, promote, demote or fire any employee.
The code that is displayed here is bad. Literally bad. I mean, it compiles and runs, of course (Everything else would be more embarassing than it already is), but there are so many ways where the system can be tricked. 
But heads up, not much ninja code is used (https://javascript.info/ninja-code)

## Errors that can be detected
* Creating a user is not checking for any existing user. A UUID is missing. This means you can create the same user multiple times
* Firing a user is done by identifying him by his firstname. This means if you have two users 'John' in your database, both get fired
* Return responses are always successful, even if no change or creation has taken place
* Creating a new resource has any types implemented -> bad coding practice
* There is no security to this open backend, even though it should. Anyone can do anything in this system
* Logging is done without timestamps. And the logged information cannot be used for backtracking performed changes to a user
* Updating a resource is done by updating the whole resource. It would be better to add different endpoints to change only a certain field
* Calculating the salary is done by a hardcoded number. As everywhere in the code, basically. That makes it hard to update certain information

## Make it run

When finished cloning to your pc, run
* npm i
* docker-compose up --build

I guess you also have to have mongodb installed....

Then you can "theoretically" navigate to http://localhost:3005/docs to see the swagger documentation and mess around with this example. For displaying the data in mongodb you can use Robo3T. Details for connecting are in the config.yml